#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool


class Period(ModelSQL, ModelView):
    _name = 'account.period'

    close_date = fields.Date('Close Date', readonly=True)

    def __init__(self):
        super(Period, self).__init__()
        self._error_messages.update({
            'open_closed_period': 'You can not reopen ' \
                    'a closed period'
            })

    def write(self, ids, vals):
        if vals.get('state') == 'open':
            for period in self.browse(ids):
                if period.state == 'close':
                    self.raise_user_error('open_closed_period')
        return super(Period, self).write(ids, vals)

    def close(self, ids):
        pool = Pool()
        journal_period_obj = pool.get('account.journal.period')
        move_obj = pool.get('account.move')
        date_obj = pool.get('ir.date')

        if isinstance(ids, (int, long)):
            ids = [ids]

        if move_obj.search([
            ('period', 'in', ids),
            ('state', '!=', 'posted'),
            ]):
            self.raise_user_error('close_period_non_posted_move')
        #First close the period to be sure
        #it will not have new journal.period created between.
        self.write(ids, {
            'state': 'close',
            'close_date': date_obj.today()
            })
        journal_period_ids = journal_period_obj.search([('period', 'in', ids)])
        journal_period_obj.close(journal_period_ids)
        return

Period()
