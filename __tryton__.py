# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Period Close Strict',
    'name_de_DE': 'Buchhaltung Buchungsperioden Irreversibler Abschluss',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Close all accounting periods strictly
    - Provides the irreversible closing of accounting periods as
      * periods
      * journal periods
      * fiscal years
''',
    'description_de_DE': '''Strikter Abschluss aller Buchhaltungszeiträume
    - Implementiert den irreversiblen Abschluss aller Arten von
      Buchungszeiträumen wie
      * Buchungszeiträume
      * Journal-Buchungszeiträume
      * Geschäftsjahre
''',
    'depends': [
        'account',
    ],
    'xml': [
        'fiscalyear.xml',
        'journal.xml',
        'period.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
