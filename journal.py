#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool


class JournalPeriod(ModelSQL, ModelView):
    _name = 'account.journal.period'

    close_date = fields.Date('Close Date', readonly=True)

    def __init__(self):
        super(JournalPeriod, self).__init__()
        self._error_messages.update({
            'open_closed_journal_period': 'You can not reopen ' \
                    'a closed journal - period',
            })

    def _check_modify(self, ids):
        move_obj = Pool().get('account.move')
        for period in self.browse(ids):
            move_ids = move_obj.search([
                ('journal', '=', period.journal.id),
                ('period', '=', period.period.id),
                ], limit=1)
            if move_ids:
                self.raise_user_error('modify_del_journal_period')
        return

    def _check(self, ids):
        return

    def close(self, ids):
        date_obj = Pool().get('ir.date')
        self.write(ids, {
            'state': 'close',
            'close_date': date_obj.today()
            })

    def write(self, ids, vals):
        if vals != {'state': 'close'} \
                and vals != {'state': 'open'}:
            values = vals.copy()
            if values.get('close_date'):
                del values['close_date']
            if values != {'state': 'close'} and vals != {'state': 'open'}:
                self._check_modify(ids)
        if vals.get('state') == 'open':
            for journal_period in self.browse(ids):
                if journal_period.state == 'close':
                    self.raise_user_error('open_closed_journal_period')
        return super(JournalPeriod, self).write(ids, vals)

JournalPeriod()
