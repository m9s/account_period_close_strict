#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pool import Pool


class FiscalYear(ModelSQL, ModelView):
    _name = 'account.fiscalyear'

    close_date = fields.Date('Close Date', readonly=True)

    def __init__(self):
        super(FiscalYear, self).__init__()
        self._error_messages.update({
            'reopen_closed_error': 'You can not reopen a closed fiscal year',
            })

    def close(self, fiscalyear_id):
        '''
        Close a fiscal year

        :param fiscalyear_id: the fiscal year id
        '''
        pool = Pool()
        period_obj = pool.get('account.period')
        account_obj = pool.get('account.account')
        date_obj = pool.get('ir.date')
        if isinstance(fiscalyear_id, list):
            fiscalyear_id = fiscalyear_id[0]

        fiscalyear = self.browse(fiscalyear_id)

        if self.search([
            ('end_date', '<=', fiscalyear.start_date),
            ('state', '=', 'open'),
            ('company', '=', fiscalyear.company.id),
            ]):
            self.raise_user_error('close_error')

        #First close the fiscalyear to be sure
        #it will not have new period created between.
        self.write(fiscalyear_id, {
            'state': 'close',
            'close_date': date_obj.today()
            })
        period_ids = period_obj.search([
            ('fiscalyear', '=', fiscalyear_id),
            ])
        period_obj.close(period_ids)
        with Transaction().set_context(date=False, fiscalyear=fiscalyear_id):
            account_ids = account_obj.search([
                ('company', '=', fiscalyear.company.id),
                ])
            for account in account_obj.browse(account_ids):
                self._process_account(account, fiscalyear)

    def reopen(self, fiscalyear_id):
        '''
        Re-opening a fiscal year is not allowed
        '''
        self.raise_user_error('reopen_closed_error')

FiscalYear()
